import { render, screen } from '@testing-library/react';
import App from './App';
import percySnapshot from "@percy/puppeteer";
import puppeteer from "puppeteer";
import { setup, teardown } from "jest-dev-server";

// global-setup.js
async function globalSetup() {
  console.log("start dev server");
  await setup({
      command: `cd ../.. & npm run start`,
      launchTimeout: 120000,
      port: 3000,
  });
  console.log("Running server!");
  // Your global setup
}

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

describe("Integration test with visual testing", () => {
    it("Loads the homepage", async function () {
        await globalSetup();
        const browser = await puppeteer.launch({ args: ["--no-sandbox"]});
        const page = await browser.newPage();
        console.log("navigate to page");
        await page.goto("http://localhost:3000", {
            waitUntil: "networkidle0",
            timeout: 60000,
        });
        console.log("make snapshot")
        await percySnapshot(page, "Start Page");
        console.log("3")
        await browser.close();
        console.log("2")
        await teardown();
        console.log("1")
    }, 600000);
});
